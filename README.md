# INSTALL NODE MODULES

run "npm install"

# BUILD PROJECT

run "npm run build"

# START PROJECT

run "npm run dev"

Note: in order to use gitlab please watch our demo video at
https://www.youtube.com/watch?v=xx9WkpT0K3E&fbclid=IwAR313JfNm_KJbMFmZl-R3-Aa6OtYXTuAILQWEi8Wo6821UfSYOpvqObE7Bs
